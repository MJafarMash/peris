#-------------------------------------------------
#
# Project created by QtCreator 2012-08-18T00:51:27
#
#-------------------------------------------------

QT       += core gui

TARGET = SimpleDrawer
TEMPLATE = app


SOURCES += main.cpp\
        drawingform.cpp \
    drawingarea.cpp \
    scale.cpp

HEADERS  += drawingform.h \
    drawingarea.h \
    scale.h

FORMS    += drawingform.ui
